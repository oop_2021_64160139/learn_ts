class Person {
    public constructor(private nameP: string) {
        this.nameP = nameP;
    }

    public getName(): string {
        return this.nameP;
    }
}


const person = new Person("jonh");
console.log(person);
console.log(person.getName())

interface Shap {
    getArea: () => number;
}


class Rectangle2 implements Shap {
    public constructor(protected readonly width: number, protected readonly heigth: number) { }
    public getArea(): number {
        return this.width * this.heigth;
    }

}


const rect: Rectangle2 = new Rectangle2(50, 10);
console.log(rect);
console.log(rect.getArea());

class Squere extends Rectangle2 {
    public constructor(width: number) {
        super(width, width)
    }
    public toString(): string {
        return `Squere[${this.width}]`
    }
}

const cs: Squere = new Squere(10);
console.log(cs)
console.log(cs.getArea());
console.log(cs.toString());

abstract class Polygon {
    public abstract gerArea(): number;
    public toString(): string {
        return `Polygon[area=${this.gerArea()}]`
    }
}

class Rectangle3 extends Polygon {
    public gerArea(): number {
        throw this.width * this.heigth;
    }
    public constructor(protected readonly width: number, protected readonly heigth: number){
        super();
    }
}

const rect1: Rectangle3 = new Rectangle3(50, 10);
console.log(rect);
console.log(rect1.toString());