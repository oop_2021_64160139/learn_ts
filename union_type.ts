function printStatusCode(code: number | String) {
    if (typeof code == "string") {
        console.log(`My status Code is ${code.toUpperCase()} ${typeof code}`);
    } else {
        console.log(`My status Code is ${code} ${typeof code}`);

    }
}
printStatusCode(404);
printStatusCode("abc");